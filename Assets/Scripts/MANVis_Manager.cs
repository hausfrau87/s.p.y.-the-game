﻿//=============================================================================
// 
// Manages the whole VR interaction.
// (c) 2016 by CGIdea / Valentin Kraft
//
//=============================================================================

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MANVis_Manager : MonoBehaviour
{

    public List<GameObject> teleportSpots;
    public float arrowGroundOffset = 0.5f;
    public float arrowScaling = 0.5f;

    private MANVis_LeftController leftController;
    private MANVis_RightController rightController;
    private List<Renderer> highlightedObjects; //containing the duplicates of the highlightable objects


    //=============================================================================
    // Initialization functions
    //=============================================================================

    void Start()
    {
        StartCoroutine(LateStart(1f));

        if (teleportSpots.Count <= 1)
            Debug.Log("[MANVisualiser] TeleportSpots list in MANVis Manager is empty or contains only one element. No arrows will be generated.");
        else
            generateSpotArrows();
    }

    private IEnumerator LateStart(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

    }

    public void RegisterRightController()
    {
        try
        {
            rightController = GetComponentInChildren<MANVis_RightController>();

            InitHighlightableObjects();
            UpdateHighlightableObjectsInController();

            Debug.Log("[MANVisualiser] Right Controller found.");
        }
        catch
        {
            Debug.LogError("[MANVisualiser] Couldn't find Right Controller!");
        }

    }

    public void RegisterLeftController()
    {
        try
        {
            leftController = GetComponentInChildren<MANVis_LeftController>();

            Debug.Log("[MANVisualiser] Left Controller found.");
        }
        catch
        {
            Debug.LogError("[MANVisualiser] Couldn't find Left Controller!");
        }

    }



    void InitHighlightableObjects()
    {
        highlightedObjects = new List<Renderer>();
        GameObject[] highlightableObjects = GameObject.FindGameObjectsWithTag("HighlightableObject");

        try
        {
            Material highlightMat = new Material(Shader.Find("3y3net/GlowVisible"));
            highlightMat.SetFloat("_Outline", rightController.highlightThickness);
            highlightMat.SetColor("_GlowColor", rightController.highlightColor);

            foreach (var obj in highlightableObjects)
            {
                //Create glowing dummyobject of every highlightable object
                GameObject duplicateObj;
                duplicateObj = GameObject.Instantiate(obj) as GameObject;
                duplicateObj.transform.parent = obj.transform.parent;
                foreach (var renderer in duplicateObj.GetComponentsInChildren<Renderer>())
                    highlightedObjects.Add(renderer);

                //Disable Colliders of actual objects so that they don't get hit by the Raycast
                Collider[] colliders = obj.GetComponentsInChildren<Collider>();
                foreach (var collider in colliders)
                    collider.enabled = false;

            }

            foreach (var renderer in highlightedObjects)
            {
                var transforms = renderer.gameObject.GetComponentsInChildren<Transform>();
                foreach (var child in transforms)
                    child.gameObject.tag = "HighlightableObject";

                //Change materials for every dummyobject to the highlight-material and add mesh colliders if necessary
                renderer.material = highlightMat;
                renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                renderer.receiveShadows = false;
                if (!renderer.gameObject.GetComponent<Collider>())
                    renderer.gameObject.AddComponent<MeshCollider>().convex = true;

            }
        }
        catch
        {
            Debug.LogError("[MANVisualiser] Couldn't find Shader for highlightable objects! (3y3net/GlowVisible)");
        }

    }

    //=============================================================================
    // Main functions
    //=============================================================================

    void UpdateHighlightableObjectsInController()
    {
        rightController.highlightedObjects = highlightedObjects;
    }

    void generateSpotArrows()
    {
        for (int i = 0; i < (teleportSpots.Count - 1); i++)
            GenerateArrowBetweenSpots(teleportSpots[i].transform.position, teleportSpots[i + 1].transform.position, 0.20f);

    }

    //=============================================================================
    // Helper functions
    //=============================================================================

    void GenerateArrowBetweenSpots(Vector3 start, Vector3 end, float dashSpacing)
    {
        Vector3 startToEnd = end - start;
        start += 0.1f * startToEnd;
        end -= 0.1f * startToEnd;
        startToEnd = end - start;

        Quaternion dashRotation = Quaternion.LookRotation(startToEnd, Vector3.up);

        GameObject parent = new GameObject();
        parent.name = "arrow";
        
        GameObject endPoint = GameObject.CreatePrimitive(PrimitiveType.Quad);
        endPoint.name = "endPoint";
        endPoint.transform.position = end + arrowGroundOffset * Vector3.up;
        endPoint.transform.localScale = Vector3.one * arrowScaling;
        endPoint.transform.rotation = dashRotation;
        endPoint.transform.Rotate(90f, 0f, 0f);
        endPoint.transform.parent = parent.transform;

        int dashCount = Mathf.RoundToInt(1f/dashSpacing);

        for(int i = 0; i <= (dashCount-1); i++)
        {
            GameObject startPoint = GameObject.CreatePrimitive(PrimitiveType.Quad);
            startPoint.name = "dash";
            startPoint.transform.position = start + startToEnd * dashSpacing * i + arrowGroundOffset * Vector3.up;
            startPoint.transform.localScale = Vector3.one * arrowScaling;
            startPoint.transform.rotation = dashRotation;
            startPoint.transform.Rotate(90f, 0f, 0f);
            startPoint.transform.parent = parent.transform;
        }
        

        
        

    }
}
