﻿using UnityEngine;
using System.Collections;

public class GestureRecognizer : MonoBehaviour
{

    public GameObject leftController;
    public GameObject rightController;
    public float minDist = 0.2f;

    private bool closeEnough = false;
    private bool trueDat = true;

    private Vector3 gestureStartPos;

    private float travelledDist = 0f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        var dist = Vector3.Distance(leftController.transform.position, rightController.transform.position);

        if (dist <= minDist)
        {
            if (!closeEnough)
            {
                gestureStartPos = leftController.transform.position;
            }

            closeEnough = trueDat;
        }
        else
            closeEnough = !trueDat;

        if (closeEnough)
        {
            if (leftController.transform.position.y > gestureStartPos.y+0.2f)
                return;

            travelledDist = gestureStartPos.y - leftController.transform.position.y;

            if (travelledDist >= 0.4f)
                Debug.Log("YUP");

            // $$$ TODO: TRACK DA POSITION OF DA BITCHES HEAD $$$
        }



    }
}
