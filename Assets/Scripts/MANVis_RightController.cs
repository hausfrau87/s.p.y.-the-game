﻿//=============================================================================
// 
// Adds Laserpointer functionality to right SteamVR Controller.
// (c) 2016 by CGIdea / Valentin Kraft
//
//=============================================================================

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MANVis_RightController : MonoBehaviour
{
    public enum AxisType
    {
        XAxis,
        ZAxis
    }

    public GameObject carModel;
    public bool showController = true;
    public bool useLaserPointer = true;
    public bool showPointerTip = true;
    public float portingSpeed = 0.15f;
    public Color pointerColor = new Color(1f, 0f, 0f);
    public float blinkTransitionSpeed = 0.6f;
    public Color highlightColor = new Color(0f, 0f, 1f);
    public float highlightThickness = 0.1f;
    public float highlightOpacity = 1.0f;
    public List<Renderer> highlightedObjects;     //gets filled by the SteamVR Manager


    //private MANVis_Manager manager;
    private SteamVR_TrackedObject trackedController;
    private SteamVR_Controller.Device device;
    private GameObject headsetCamera;
    private GameObject headsetCameraRig;
    private float headsetCameraRigInitialYPosition;

    private GameObject controllerAttachpoint;
    private Rigidbody controllerAttachPoint;
    private FixedJoint controllerAttachJoint;
    private Transform pointerContactTarget = null;
    private GameObject pointer;
    private GameObject pointerTip;
    private GameObject pointerGlow;
    private GameObject controllerModel;
    
    private Vector3 teleportLocation;
    private Vector3 pointerTipScale = new Vector3(0.05f, 0.05f, 0.05f);
    private float pointerContactDistance = 0f;
    private float pointerThickness = 0.002f;
    private float pointerMaxLength = 100f;
    private AxisType pointerFacingAxis = AxisType.ZAxis;
    
    //Unused
    private GameObject canGrabObject;
    private Color[] canGrabObjectOriginalColors;
    private GameObject previousGrabbedObject;
    private bool highlightGrabbableObject = true;   //changed to private (not used atm)
    private Color grabObjectHightlightColor;        //changed to private (not used atm)

    


    /*          S H E E P
     * 
     *                       __
            ,'```--'''  ``-''-.
          ,'            ,-- ,-'.
         (//            `"'| 'a \
           |    `;         |--._/
           \    _;-._,    /
            \__/\\   \__,'
             ||  `'   \|\\
             \\        \\`'
              `'        `'
     */

    //=============================================================================
    // Initialization functions
    //=============================================================================

    void Awake()
    {
        try
        {
            trackedController = GetComponent<SteamVR_TrackedObject>();
        }
        catch
        {
            Debug.LogWarning("[MANVisualiser] There is no SteamVR_TrackedObject script assigned to the current controller!");
        }

        try
        {
            //manager = transform.parent.GetComponent<MANVis_Manager>();
            //manager.RegisterRightController();
        }
        catch
        {
            Debug.LogWarning("[MANVisualiser] There is no MANVis manager script assigned to the camera rig!");
        }
    }

    void Start()
    {
        InitController();
        InitHeadsetReferencePoint();


        if (carModel == null)
            Debug.LogWarning("[MANVisualiser] Car model not assigned!");

    }

    void InitController()
    {
        //controllerAttachPoint = transform.GetChild(0).Find("tip").GetChild(0).GetComponent<Rigidbody>();
        try
        {
            controllerModel = transform.GetChild(0).gameObject;
        }
        catch
        {
            Debug.LogError("[MANVisualiser] Couldn't find controller model!");
        }

        InitPointer();
    }

    void InitPointer()
    {
        Material newMaterial = new Material(Shader.Find("Unlit/Color"));
        newMaterial.SetColor("_Color", pointerColor);

        controllerAttachpoint = new GameObject();
        controllerAttachpoint.name = "ControllerAttachPoint";
        controllerAttachpoint.transform.parent = this.transform;
        controllerAttachpoint.transform.localPosition = Vector3.zero;

        pointer = GameObject.CreatePrimitive(PrimitiveType.Cube);
        pointer.name = "Pointer";
        pointer.transform.parent = controllerAttachpoint.transform;
        pointer.GetComponent<MeshRenderer>().material = newMaterial;
        pointer.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        pointer.GetComponent<BoxCollider>().isTrigger = true;
        pointer.AddComponent<Rigidbody>().isKinematic = true;
        pointer.layer = 2;

        pointerGlow = GameObject.CreatePrimitive(PrimitiveType.Cube);
        pointerGlow.name = "PointerGlow";
        pointerGlow.transform.parent = controllerAttachpoint.transform;
        pointerGlow.layer = 2;
        try
        {
            Material glowMat = new Material(Shader.Find("3y3net/GlowVisible"));
            glowMat.SetFloat("_Outline", 0.0f);                                             //turned off - #ToDo: pointerGlow
            glowMat.SetColor("_GlowColor", pointerColor);
            pointerGlow.GetComponent<MeshRenderer>().material = glowMat;
            pointerGlow.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        }
        catch
        {
            Debug.LogError("[MANVisualiser] Couldn't find Shader for highlightable objects! (3y3net/GlowVisible)");
        }

        pointerTip = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        pointerTip.transform.parent = controllerAttachpoint.transform;
        pointerTip.GetComponent<MeshRenderer>().material = newMaterial;
        pointerTip.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        pointerTip.transform.localScale = pointerTipScale;

        pointerTip.GetComponent<SphereCollider>().isTrigger = true;
        pointerTip.AddComponent<Rigidbody>().isKinematic = true;
        pointerTip.layer = 2;

        SetPointerTransform(pointerMaxLength, pointerThickness);
        TogglePointer(false);
    }

    void InitHeadsetReferencePoint()
    {
        //Transform eyeCamera = GameObject.FindObjectOfType<SteamVR_Camera>().GetComponent<Transform>();
        headsetCamera = GameObject.FindObjectOfType<SteamVR_Camera>().gameObject;
        if (headsetCamera == null)
            Debug.LogError("[MANVisualiser] Couldn't find SteamVR camera! Please import SteamVR Camera Rig Prefab.");
        // The referece point for the camera is two levels up from the SteamVR_Camera
        //HeadsetCameraRig = eyeCamera.parent.parent;
        headsetCameraRig = headsetCamera.transform.parent.gameObject;
        headsetCameraRigInitialYPosition = headsetCameraRig.transform.position.y;
    }



    //=============================================================================
    // Main functions
    //=============================================================================

    void ChangeCarColor()
    {
        //#ToDo: besser machen? ^^
        try
        {
            var renderers = carModel.GetComponentsInChildren<Renderer>();
            foreach (var renderer in renderers)
                renderer.material.SetColor("_Color", pointerContactTarget.GetComponent<Renderer>().material.GetColor("_Color"));
        }
        catch
        {
            Debug.LogError("[MANVisualiser] Couldn't change car's material color! There seem to be wrong materials or no car model attached.");
        }
    }
    void SetPointerTransform(float setLength, float setThicknes)
    {
        //if the additional decimal isn't added then the beam position glitches
        float beamPosition = setLength / (2 + 0.00001f);

        if (pointerFacingAxis == AxisType.XAxis)
        {
            pointer.transform.localScale = new Vector3(setLength, setThicknes, setThicknes);
            pointer.transform.localPosition = new Vector3(beamPosition, 0f, 0f);
            pointerTip.transform.localPosition = new Vector3(setLength - (pointerTip.transform.localScale.x / 2), 0f, 0f);
        }
        else
        {
            pointer.transform.localScale = new Vector3(setThicknes, setThicknes, setLength);
            pointer.transform.localPosition = new Vector3(0f, 0f, beamPosition);
            pointerTip.transform.localPosition = new Vector3(0f, 0f, setLength - (pointerTip.transform.localScale.z / 2));
        }

        pointerGlow.transform.localScale = pointer.transform.localScale;
        pointerGlow.transform.localPosition = pointer.transform.localPosition;

        teleportLocation = pointerTip.transform.position;
    }

    float GetPointerBeamLength(bool hasRayHit, RaycastHit collidedWith)
    {
        float actualLength = pointerMaxLength;

        //reset if beam not hitting or hitting new target
        if (!hasRayHit || (pointerContactTarget && pointerContactTarget != collidedWith.transform))
        {
            pointerContactDistance = 0f;
            pointerContactTarget = null;
        }

        //check if beam has hit a new target
        if (hasRayHit)
        {
            pointerContactDistance = collidedWith.distance;
            pointerContactTarget = collidedWith.transform;
        }

        //adjust beam length if something is blocking it
        if (hasRayHit && pointerContactDistance < pointerMaxLength)
            actualLength = pointerContactDistance;


        return actualLength;
    }

    void TogglePointer(bool state)
    {
        pointer.gameObject.SetActive(state);
        pointerGlow.SetActive(state);
        bool tipState = (showPointerTip ? state : false);
        pointerTip.gameObject.SetActive(tipState);
    }

    void UpdatePointer()
    {
        //------------------------------------------------------
        // Here goes the laserpointer interaction logic
        //------------------------------------------------------
        if (pointerContactTarget != null)
        {
            //if (device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
            if (device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger) && pointerContactTarget.gameObject.tag == "Enemy")
                Teleport();
            if (device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger) && pointerContactTarget.gameObject.tag == "ColorChanger")
                ChangeCarColor();
            if (pointerContactTarget.gameObject.tag == "HighlightableObject")
                HighlightObject();
        }

        Ray pointerRaycast = new Ray(transform.position, transform.forward);
        RaycastHit pointerCollidedWith;
        bool rayHit = Physics.Raycast(pointerRaycast, out pointerCollidedWith);
        float pointerBeamLength = GetPointerBeamLength(rayHit, pointerCollidedWith);
        SetPointerTransform(pointerBeamLength, pointerThickness);

    }

    void Teleport()
    {
        SteamVR_Fade.Start(Color.black, 0);
        SteamVR_Fade.Start(Color.clear, blinkTransitionSpeed);
        StartCoroutine(MoveObject(headsetCameraRig, new Vector3(pointerContactTarget.position.x, headsetCameraRigInitialYPosition, pointerContactTarget.position.z), portingSpeed, false));
        //StartCoroutine(MoveObject(headsetCameraRig, new Vector3(pointerTip.transform.position.x, headsetCameraRigInitialYPosition, pointerTip.transform.position.z), portingSpeed, false));
        //headsetCameraRig.position = new Vector3(pointerContactTarget.position.x, headsetCameraRigInitialYPosition, pointerContactTarget.position.z);
        //HeadsetCamera.transform.position = new Vector3(TeleportLocation.x, HeadsetCameraRigInitialYPosition, TeleportLocation.z);
    }

    void HighlightObject()
    {
        pointerContactTarget.GetComponent<Renderer>().material.SetFloat("_Opacity", highlightOpacity);
    }

    void SnapCanGrabObjectToController(GameObject obj)
    {
        obj.transform.position = controllerAttachPoint.transform.position;

        controllerAttachJoint = obj.AddComponent<FixedJoint>();
        controllerAttachJoint.connectedBody = controllerAttachPoint;
        ToggleGrabbableObjectHighlight(false);
    }

    Rigidbody ReleaseGrabbedObjectFromController()
    {
        var jointGameObject = controllerAttachJoint.gameObject;
        var rigidbody = jointGameObject.GetComponent<Rigidbody>();
        Object.DestroyImmediate(controllerAttachJoint);
        controllerAttachJoint = null;

        return rigidbody;
    }

    void ThrowReleasedObject(Rigidbody rb)
    {
        var origin = trackedController.origin ? trackedController.origin : trackedController.transform.parent;
        if (origin != null)
        {
            rb.velocity = origin.TransformVector(device.velocity);
            rb.angularVelocity = origin.TransformVector(device.angularVelocity);
        }
        else
        {
            rb.velocity = device.velocity;
            rb.angularVelocity = device.angularVelocity;
        }

        rb.maxAngularVelocity = rb.angularVelocity.magnitude;
    }

    void RecallPreviousGrabbedObject()
    {
        if (previousGrabbedObject != null && device.GetTouchDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
        {
            previousGrabbedObject.transform.position = controllerAttachPoint.transform.position;
            previousGrabbedObject.transform.rotation = controllerAttachPoint.transform.rotation;
            var rb = previousGrabbedObject.GetComponent<Rigidbody>();
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            rb.maxAngularVelocity = 0f;
        }
    }

    void UpdateGrabbableObjects()
    {
        if (canGrabObject != null)
        {
            if (controllerAttachJoint == null && device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
            {
                previousGrabbedObject = canGrabObject;
                SnapCanGrabObjectToController(canGrabObject);
            }
            else if (controllerAttachJoint != null && device.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger))
            {
                Rigidbody releasedObjectRigidBody = ReleaseGrabbedObjectFromController();
                ThrowReleasedObject(releasedObjectRigidBody);
            }
        }
    }

    void UpdateHighlightedObjects()
    {
        foreach (var renderer in highlightedObjects)
            if ((pointerContactTarget != null && renderer != pointerContactTarget.GetComponent<Renderer>()) || pointerContactTarget == null)
                renderer.material.SetFloat("_Opacity", 0.0f);
    }


    void ChangeObjectColor(GameObject obj, Color[] colors)
    {
        Renderer[] rendererArray = GetObjectRendererArray(obj);
        int i = 0;
        foreach (Renderer renderer in rendererArray)
        {
            renderer.material.color = colors[i];
            i++;
        }
    }

    void ToggleGrabbableObjectHighlight(bool highlightObject)
    {
        if (highlightGrabbableObject && canGrabObject != null)
        {
            if (highlightObject)
            {
                var colorArray = BuildObjectColorArray(canGrabObject, grabObjectHightlightColor);
                ChangeObjectColor(canGrabObject, colorArray);
            }
            else
            {
                ChangeObjectColor(canGrabObject, canGrabObjectOriginalColors);
            }
        }
    }

    //=============================================================================
    // Standard functions
    //=============================================================================

    void FixedUpdate()
    {
        try
        {
            device = SteamVR_Controller.Input((int)trackedController.index);
        }
        catch
        {
            Debug.LogError("[MANVisualiser] Couldn't find SteamVR Controller!");
        }

        //RecallPreviousGrabbedObject();
        //UpdateGrabbableObjects();
        UpdateHighlightedObjects();
    }

    void Update()
    {
        if (controllerModel != null)
        {
            if (pointer.activeSelf != useLaserPointer)
                TogglePointer(useLaserPointer);

            if (controllerModel.activeSelf != showController)
                controllerModel.SetActive(!controllerModel.activeSelf);

            if (showController)
                useLaserPointer = true;
            if (!showController)
                useLaserPointer = false;

            if (useLaserPointer)
                UpdatePointer();

            if (device.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
                showController = !showController;
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Grabbable")
        {
            if (canGrabObject == null)
            {
                canGrabObjectOriginalColors = StoreObjectOriginalColors(collider.gameObject);
            }
            canGrabObject = collider.gameObject;
            ToggleGrabbableObjectHighlight(true);
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "Grabbable")
        {
            ToggleGrabbableObjectHighlight(false);
            canGrabObject = null;
        }
    }


    //=============================================================================
    // Helper functions
    //=============================================================================

    Renderer[] GetObjectRendererArray(GameObject obj)
    {
        return (obj.GetComponents<Renderer>().Length > 0 ? obj.GetComponents<Renderer>() : obj.GetComponentsInChildren<Renderer>());
    }

    Color[] BuildObjectColorArray(GameObject obj, Color defaultColor)
    {
        Renderer[] rendererArray = GetObjectRendererArray(obj);

        int length = rendererArray.Length;

        Color[] colors = new Color[length];
        for (int i = 0; i < length; i++)
        {
            colors[i] = defaultColor;
        }
        return colors;
    }

    Color[] StoreObjectOriginalColors(GameObject obj)
    {
        Renderer[] rendererArray = GetObjectRendererArray(obj);

        int length = rendererArray.Length;
        Color[] colors = new Color[length];

        for (int i = 0; i < length; i++)
        {
            var renderer = rendererArray[i];
            colors[i] = renderer.material.color;
        }

        return colors;
    }

    ///<summary>
    ///Smooth movement of the given object to the destination. If fixedSpeed is true, the "inSeconds"-variable represents the speed value, when false the object moves in the given amount of seconds. 
    ///</summary>
    private IEnumerator MoveObject(GameObject obj, Vector3 target, float inSeconds, bool fixedSpeed)
    {
        Vector3 startPos = obj.transform.position;
        float dist = Vector3.Distance(startPos, target);
        float t = 0f;

        while (t <= 1f)
        {
            if (fixedSpeed)
                t += Time.deltaTime / inSeconds / dist * 10f;
            else
                t += Time.deltaTime / inSeconds;

            obj.transform.position = Vector3.Lerp(startPos, target, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }

        obj.transform.position = target;
    }
}
