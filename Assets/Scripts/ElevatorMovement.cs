﻿using UnityEngine;
using System.Collections;

public class ElevatorMovement : MonoBehaviour
{

    public bool elevating = false;
    public float duration = 2.5f;
    public float height = 1.5f;

    private bool shouldElevate = false;
    private bool goUp = true;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider col)
    {

        if (col.gameObject.tag == "Player")
        {
            shouldElevate = true;
            if (goUp)
                StartCoroutine(ElevateUp());
            else
                StartCoroutine(ElevateDown());
        }
        else
            shouldElevate = false;

    }


    private IEnumerator ElevateUp()
    {
        yield return new WaitForSeconds(3f);

        if (shouldElevate && !elevating)
        {
            StartCoroutine(MoveObject(this.gameObject, this.transform.position + height * Vector3.up, duration, false));
            elevating = true;
        }
    }

    private IEnumerator ElevateDown()
    {
        yield return new WaitForSeconds(3f);

        if (shouldElevate && elevating)
        {
            StartCoroutine(MoveObject(this.gameObject, this.transform.position - height * Vector3.up, duration, false));
            elevating = false;
        }
    }

    ///<summary>
    ///Smooth movement of the given object to the destination. If fixedSpeed is true, the "inSeconds"-variable represents the speed value, when false the object moves in the given amount of seconds. 
    ///</summary>
    private IEnumerator MoveObject(GameObject obj, Vector3 target, float inSeconds, bool fixedSpeed)
    {
        Vector3 startPos = obj.transform.position;
        float dist = Vector3.Distance(startPos, target);
        float t = 0f;

        while (t <= 1f)
        {
            if (fixedSpeed)
                t += Time.deltaTime / inSeconds / dist * 10f;
            else
                t += Time.deltaTime / inSeconds;

            obj.transform.position = Vector3.Lerp(startPos, target, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }

        obj.transform.position = target;

        yield return new WaitForSeconds(1.5f);
        goUp = !goUp;
    }
}
