﻿//=============================================================================
// 
// Adds interface functionality to left SteamVR Controller.
// (c) 2016 by CGIdea / Valentin Kraft
//
//=============================================================================

using UnityEngine;
//using UnityStandardAssets.CrossPlatformInput;
//using System.Collections;

public class MANVis_LeftController : MonoBehaviour
{

    public bool showController = true;
    public bool useInterface = true;
    //public GameObject interfacePrefab;

    private SteamVR_TrackedObject trackedController;
    private SteamVR_Controller.Device device;

    private GameObject controllerModel;

    private GameObject headsetCameraRig;
    private float headsetCameraRigInitialYPosition;
    private GameObject headsetCamera;
    private GameObject controllerAttachpoint;
    private GameObject interfaceObj;



    /*          S H E E P
     * 
     *                       __
            ,'```--'''  ``-''-.
          ,'            ,-- ,-'.
         (//            `"'| 'a \
           |    `;         |--._/
           \    _;-._,    /
            \__/\\   \__,'
             ||  `'   \|\\
             \\        \\`'
              `'        `'
     */

    //=============================================================================
    // Initialization functions
    //=============================================================================

    void Awake()
    {
        try
        {
            trackedController = GetComponent<SteamVR_TrackedObject>();
        }
        catch
        {
            Debug.LogWarning("[MANVisualiser] There is no SteamVR_TrackedObject script assigned to the current controller!");
        }

    }

    void Start()
    {
        InitController();
        InitHeadsetReferencePoint();
    }

    void InitController()
    {
        //controllerAttachPoint = transform.GetChild(0).Find("tip").GetChild(0).GetComponent<Rigidbody>();
        try
        {
            controllerModel = transform.GetChild(0).gameObject;
        }
        catch
        {
            Debug.LogError("[MANVisualiser] Couldn't find controller model!");
        }

        //if (interfacePrefab == null)
        //    Debug.LogWarning("[MANVisualiser] Interface prefab/model is missing!");

        controllerAttachpoint = new GameObject();
        controllerAttachpoint.name = "ControllerAttachPoint";
        controllerAttachpoint.transform.parent = this.transform;
        controllerAttachpoint.transform.localPosition = Vector3.zero;
        //GameObject interfaceObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
        //interfaceObj.transform.localScale = new Vector3(0.15f, 0.02f, 0.15f);
        //interfaceObj = GameObject.Instantiate(interfacePrefab) as GameObject;
        //interfaceObj.name = "Interface";
        //interfaceObj.transform.parent = controllerAttachpoint.transform;
        //interfaceObj.transform.localPosition = Vector3.zero;
    }

    void InitHeadsetReferencePoint()
    {
        //Transform eyeCamera = GameObject.FindObjectOfType<SteamVR_Camera>().GetComponent<Transform>();
        headsetCamera = GameObject.FindObjectOfType<SteamVR_Camera>().gameObject;
        if (headsetCamera == null)
            Debug.LogError("[MANVisualiser] Couldn't find SteamVR camera! Please import SteamVr Camera Rig Prefab.");
        // The referece point for the camera is two levels up from the SteamVR_Camera
        //HeadsetCameraRig = eyeCamera.parent.parent;
        headsetCameraRig = headsetCamera.transform.parent.gameObject;
        headsetCameraRigInitialYPosition = headsetCameraRig.transform.position.y;
    }

    //=============================================================================
    // Main functions
    //=============================================================================


    //=============================================================================
    // Standard functions
    //=============================================================================

    void FixedUpdate()
    {
        try
        {
            device = SteamVR_Controller.Input((int)trackedController.index);
        }
        catch
        {
            Debug.LogError("[MANVisualiser] Couldn't find SteamVR Controller!");
        }

    }

    void Update()
    {

        /*if (interfaceObj != null && controllerModel != null)
        {
            if (controllerModel.activeSelf != showController)
                controllerModel.SetActive(!controllerModel.activeSelf);

            if (device.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
                showController = !showController;

            if (showController)
                useInterface = true;
            if (!showController)
                useInterface = false;

            if (interfaceObj.activeSelf != useInterface)
                interfaceObj.SetActive(!interfaceObj.activeSelf);
        }*/
        float sensitivity = 0.03f;
        Vector2 touchpad = device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
        //touchpad.y *= -1f;
        if (headsetCamera.transform.forward.z < 0f)
            touchpad.x *= -1f;
        headsetCameraRig.transform.position += new Vector3(touchpad.x * sensitivity, 0f, 0f);
        headsetCameraRig.transform.Translate(new Vector3(headsetCamera.transform.forward.x, 0f, headsetCamera.transform.forward.z) * touchpad.y * sensitivity);
        //headsetCameraRig.transform.Rotate(Vector3.up, touchpad.y);

    }

    public Vector2 GetTouchpadValues()
    {
        return device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
    }


    //=============================================================================
    // Helper functions
    //=============================================================================



}
