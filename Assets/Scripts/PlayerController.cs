﻿//=============================================================================
// 
// Adds interface functionality to left SteamVR Controller.
// (c) 2016 by CGIdea / Valentin Kraft
//
//=============================================================================

using UnityEngine;
//using System.Collections;

public class PlayerController : MonoBehaviour
{

    //public bool showController = true;
    //public bool useInterface = true;
    //public GameObject interfacePrefab;

    //private SteamVR_TrackedObject trackedController;
    //private SteamVR_Controller.Device device;

    private GameObject controllerModel;

    private GameObject headsetCameraRig;
    private float headsetCameraRigInitialYPosition;
    private GameObject headsetCamera;
    private GameObject controllerAttachpoint;
    private GameObject interfaceObj;
    private Vector3 lastPosition;

    private GameObject currentCollider;

    //=============================================================================
    // Initialization functions
    //=============================================================================


    void Awake()
    {
        /*try
        {
            trackedController = GetComponent<SteamVR_TrackedObject>();
        }
        catch
        {
            Debug.LogWarning("[MANVisualiser] There is no SteamVR_TrackedObject script assigned to the current controller!");
        }*/


        lastPosition = transform.position;

    }

    void Start()
    {
        //InitController();
        InitHeadsetReferencePoint();
    }

    void InitController()
    {
        //controllerAttachPoint = transform.GetChild(0).Find("tip").GetChild(0).GetComponent<Rigidbody>();
        try
        {
            controllerModel = transform.GetChild(0).gameObject;
        }
        catch
        {
            Debug.LogError("[MANVisualiser] Couldn't find controller model!");
        }

        //if (interfacePrefab == null)
        //    Debug.LogWarning("[MANVisualiser] Interface prefab/model is missing!");

        controllerAttachpoint = new GameObject();
        controllerAttachpoint.name = "ControllerAttachPoint";
        controllerAttachpoint.transform.parent = this.transform;
        controllerAttachpoint.transform.localPosition = Vector3.zero;
        //GameObject interfaceObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
        //interfaceObj.transform.localScale = new Vector3(0.15f, 0.02f, 0.15f);
        //interfaceObj = GameObject.Instantiate(interfacePrefab) as GameObject;
        //interfaceObj.name = "Interface";
        //interfaceObj.transform.parent = controllerAttachpoint.transform;
        //interfaceObj.transform.localPosition = Vector3.zero;
    }

    void InitHeadsetReferencePoint()
    {
        //Transform eyeCamera = GameObject.FindObjectOfType<SteamVR_Camera>().GetComponent<Transform>();
        headsetCamera = GameObject.FindObjectOfType<SteamVR_Camera>().gameObject;
        if (headsetCamera == null)
            Debug.LogError("[MANVisualiser] Couldn't find SteamVR camera! Please import SteamVr Camera Rig Prefab.");
        // The referece point for the camera is two levels up from the SteamVR_Camera
        //HeadsetCameraRig = eyeCamera.parent.parent;
        headsetCameraRig = headsetCamera.transform.parent.gameObject;
        headsetCameraRigInitialYPosition = headsetCameraRig.transform.position.y;
    }

    //=============================================================================
    // Main functions
    //=============================================================================


    //=============================================================================
    // Standard functions
    //=============================================================================

    /*void FixedUpdate()
    {
        try
        {
            device = SteamVR_Controller.Input((int)trackedController.index);
        }
        catch
        {
            Debug.LogError("[MANVisualiser] Couldn't find SteamVR Controller!");
        }

    }*/

    void Update()
    {
        transform.position = headsetCamera.transform.position;
        Vector3 deltaPos = transform.position - lastPosition;

        try
        {
            if (currentCollider.tag == "Stairs")
            {
                //headsetCameraRig.transform.position += deltaPos;
                headsetCameraRig.transform.position += new Vector3(0f, deltaPos.y, 0f);
                
            }
        }
        catch { }


        lastPosition = transform.position;
    }

    void OnTriggerEnter(Collider col)
    {
        currentCollider = col.gameObject;
        //Debug.Log(currentCollider);
    }

    void OnCollisionEnter(Collider col)
    {
        currentCollider = col.gameObject;
        //Debug.Log(currentCollider);
    }

    /*public Vector2 GetTouchpadValues()
    {
        return device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
    }*/


    //=============================================================================
    // Helper functions
    //=============================================================================



}
